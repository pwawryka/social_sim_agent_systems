from random import sample, uniform, random


USERS_COUNT = 20
USER_FOLLOWERS = (1, 3)
USER_LIKES = (1, 3)
TAGS = [
    'football', 'music', 'politics', 'learning', 'poker', 
    'funny', 'movies', 'basketball', 'tennis', 'games', 'computers',
    'lol', 'titanic', 'school', 'university', 'python', 'scala',
    'gta', 'medieval', 'sword', 'gothic', 'ship', 'minecraft'
][:10]

def cmd(args):
    return '#'.join(map(str, args)) + '\n'

def randt(tup):
    return int(uniform(tup[0], tup[1] + 1))


users = {}
for id in range(USERS_COUNT):
    user_tags = sample(TAGS, randt(USER_LIKES))
    users[id] = {
        'id': id,
        'name': 'User_' + str(id),
        'tags': [(tag, round(random(), 2)) for tag in user_tags]
    }

for id, user in users.items():
    u_fol = range(USERS_COUNT)
    u_fol.pop(id)
    user['followers'] = []
    for i in range(randt(USER_FOLLOWERS)):
        user['followers'].append(u_fol.pop(randt((0, len(u_fol)-1))))

for id, user in users.items():
    for tag in user['tags']:
        user.setdefault('messages', []).append([tag[0]])

with open('example.txt', 'w+') as f:
    for id, user in users.items():
        user_tags = ','.join(['{0} {1}'.format(*tag) for tag in user['tags']])
        _cmd = cmd(['create_user', id, user['name'], user_tags, 1])
        f.write(_cmd)
    for id, user in users.items():
        for fol in user['followers']:
            _cmd = cmd(['add_follower', id, fol, 1])
            f.write(_cmd)
    for id, user in users.items():
        for msg in user['messages']:
            _cmd = cmd(['add_message', id, ','.join(msg), 1])
            f.write(_cmd)