import matplotlib.pyplot as plt
import networkx as nx
import numpy as np


class Node(object):
    def __init__(self, id, tags=None, followers=None):
        self.id = id
        self.tags = tags or []
        self.followers = followers or []

    def __repr__(self):
        return str(self.id)

    def show(self):
        self.tags = list(set(self.tags))
        # print 'Node id:', self.id
        # print 'tags:'
        # for fol in self.tags:
        #   print '\t', fol
        pass

def rand_range(_range):
    return int(np.random.uniform(*_range))


def create_nodes(n):
    return [Node(i) for i in range(1, n+1)]


def create_connections(nodes, connection_range):
    for node in nodes:
        others = filter(lambda n: n.id != node.id, nodes)
        cons = rand_range(connection_range)
        for i in range(cons):
            index = rand_range((0, len(others)))
            fol = others.pop(index)
            node.followers.append(fol)


def randomize_nodes(nodes, n, cats):
    groups = [[] for i in range(n)]
    for node in nodes:
        index = rand_range((0, n))
        groups[index].append(node)
    for i, group in enumerate(groups):
        gcat = cats[i][0]
        for node in group:
            node.tags.append(gcat)
            if len(cats[i]) > 1:
                j = rand_range((1, len(cats[i])-1))
                node.tags.append(cats[i][j])
    return groups


def connect_groups(groups, connection_range):
    for i, group in enumerate(groups):
        others = groups[:i] + groups[i+1:]
        for other in others:
            this = rand_range((0, len(group)))
            that = rand_range((0, len(other)))
            group[this].followers.append(other[that])


def create_and_draw_graph(nodes):
    graph = nx.DiGraph()
    for node in nodes:
        graph.add_node(node.id)
        for fol in node.followers:
            graph.add_edge(node.id, fol.id)
    pos = nx.spring_layout(graph)
    nx.draw(graph, pos=pos)
    plt.show()


def cmd(args):
    return '#'.join(map(str, args)) + '\n'
    

if __name__ == '__main__':
    NODES = 200
    GROUPS = 7
    CONS = (7, 10)
    GROUP_CONS = (4, 9)
    CATEGORIES = [
        ['sport', 'football', 'basketball', 'tennis', 'baseball', 'swimming'], 
        ['architecture', 'buildings', 'interior', 'castles', 'churches'], 
        ['politics', 'democracy', 'conservative', 'anarchy'], 
        ['music', 'rock', 'pop', 'metal', 'blues', 'classic', 'techno', 'dubstep'], 
        ['movies', 'musical', 'action', 'thriller', 'horror', 'drama', 'comedy'],
        ['games', 'sport_games', 'fps', 'action', 'rpg', 'strategy'], 
        ['funny']
    ]
    
    nodes = create_nodes(NODES)
    groups = randomize_nodes(nodes, GROUPS, CATEGORIES)
    for _nodes in groups:
        create_connections(_nodes, CONS)
    connect_groups(groups, GROUP_CONS)
    for node in nodes:
        node.show()
    create_and_draw_graph(nodes)

    with open('example.txt', 'w+') as f:
        for user in nodes:
            user_tags = ','.join(['{0} {1}'.format(tag, round(np.random.uniform(), 2)) for tag in user.tags])
            _cmd = cmd(['create_user', user.id, 'User_%d' % user.id, user_tags, 1])
            f.write(_cmd)
        for user in nodes:
            print len(user.followers)
            for fol in user.followers:
                _cmd = cmd(['add_follower', user.id, fol.id, 1])
                f.write(_cmd)
