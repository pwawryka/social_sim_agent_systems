import sys
import matplotlib.pyplot as plt
import networkx as nx


def get_users():
	with open('output.txt', 'r') as f:
		data = f.readlines()

	users = {int(line.split()[1]): {'id': int(line.split()[1]), 'name': line.split()[2]} for line in data if line.startswith('user')}
	for line in data:
		row = line.split()
		if line.startswith('follow'):
			users[int(row[1])].setdefault('follow', []).append(int(row[2]))
		if line.startswith('message_add'):
			users[int(row[2])].setdefault('msg', []).append(int(row[1]))
		if line.startswith('message_show'):
			users[int(row[2])].setdefault('msg_show', []).append(int(row[1]))
	
	return users

def get_messages(users):
	msg = {}
	for uid, u in users.items():
		for m in u.get('msg_show', []):
			msg.setdefault(m, set()).add(uid)
	return msg

def get_message_owners(users):
	msg = {}
	for uid, u in users.items():
		for m in u.get('msg', []):
			msg[m] = uid
	return msg


users = get_users()
msg = get_messages(users)
mos = get_message_owners(users)

mid = int(sys.argv[1])

g = nx.DiGraph()
for uid, u in users.items():
	g.add_node(uid)
	for fid in u.get('follow', []):
		g.add_edge(uid, fid)

print msg[mid], mos[mid]

node_color = [0.5 if node in msg[mid] else 0 for node in g.nodes()]
node_color[mos[mid]] = 1

pos = nx.spring_layout(g)
nx.draw(g, pos=pos, node_color=node_color)
plt.show()
