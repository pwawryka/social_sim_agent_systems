import numpy as np
import matplotlib.pyplot as plt
import networkx as nx


class Data(object):
	def __init__(self, users, followers, messages, message_viewers):
		self.users = users
		self.followers = followers
		self.messages = messages
		self.message_viewers = message_viewers


def get_input(filename):
	users, followers, messages, message_viewers = {}, {}, {}, {}
	with open(filename, 'r') as f:
		for line in f.readlines():
			row = line.split()
			if row[0] == 'user':
				users[int(row[1])] = {'id': int(row[1]), 'name': row[2]}
			elif row[0] == 'follow':
				followers.setdefault(int(row[1]), []).append(int(row[2]))
			elif row[0] == 'message_add':
				messages[int(row[1])] = {'id': int(row[1]), 'owner': int(row[2])}
			elif row[0] == 'message_show':
				message_viewers.setdefault(int(row[1]), []).append(int(row[2]))
			else:
				continue
	
	return users, followers, messages, message_viewers


def print_stats(data):
	messages_stats = sorted([(k, len(v)) for k,v in data.message_viewers.items()], key=lambda e: -e[1])
	avg_views_per_msg = np.average([msg[1] for msg in messages_stats])
	print '#######################################'
	print 'Users: ', len(data.users)
	print 'Messages: ', len(data.messages)
	print 'Connections: ', sum([len(v) for k, v in data.followers.items()])
	print 'Most viewed message: ID = ', messages_stats[0][0], '| views = ', messages_stats[0][1]
	print 'Avg views per messsage: ', avg_views_per_msg
	print '#######################################'


def show_graph(data, mid):
	owner = data.messages[mid]['owner']

	graph = nx.DiGraph()
	for user in data.users.values():
		graph.add_node(user['id'])
	for follower_id, followed in data.followers.items():
		for fid in followed:
			graph.add_edge(follower_id, fid)

	labels = {u['id']: u['name'] for u in data.users.values()}
	node_color = [0 for u in sorted([u['id'] for u in data.users.values()])]
	node_color[owner] = 1
	for viewer in data.message_viewers[mid]:
		node_color[viewer] = 0.5
	
	pos = nx.spring_layout(graph)
	nx.draw(graph, pos=pos, labels=labels, node_color=node_color)
	plt.show()


def show_message_stats(data, mid):
	msg = data.messages[mid]
	owner = msg['owner']
	# __tmp = []
	# for u, f in data.followers.items():
	# 	if owner in f:
	# 		__tmp.append(u)
	print '#######################################'
	print 'Owner: ', data.users[owner]
	# print 'Followers: ', __tmp
	print 'Followers: ', data.followers[owner]
	print 'Message viewers: ', data.message_viewers[mid]
	# print 'Tags: ', msg.get('tags', [])
	print '#######################################'
	show_graph(data, mid)


def loop(command, data):
	args = command.split()
	if args[0] == 'stats':
		print_stats(data)
	if args[0] == 'msg' and len(args) > 1:
		show_message_stats(data, int(args[1]))

data = Data(*get_input('output.txt'))
command = 'stats'
while command not in ('quit', 'q', 'exit', 'close'):
	loop(command, data)
	command = raw_input('>>> ')