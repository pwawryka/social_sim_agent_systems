object Config {
  implicit val generateMessages = true
  implicit val generatedMessagesSleepTime = 10

  implicit val generateRelations = false
  implicit val relationProbability = 0.3
}
