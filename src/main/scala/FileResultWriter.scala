import java.io.FileWriter

class FileResultWriter extends IResultWriter {
  val filename = "output/example.txt"
  override def write(data: String) = {
    val writer = new FileWriter(filename, true)
    writer.write(data + "\n")
    writer.close()
  }
}
