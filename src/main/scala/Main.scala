import akka.actor._

object Main extends App {
  val inputFile = "input/example.txt"
  val outputFile = "output/example.txt"
  val system = ActorSystem("SocialSim")
  val master = system.actorOf(Props[Master], name = "Master")
  init()

  def init() = {
    master ! Master.Init(inputFile)
  }

  def shutdown() = { master ! Master.Shutdown }
}
