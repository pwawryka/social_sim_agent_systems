import scala.collection.mutable.Map
import akka.actor._

object Master {
  case class Init(filename: String)
  case object Shutdown
}

class Master extends Actor {
  import Master._

  val users = Map[Int, ActorRef]()
  var result: ActorRef = context.actorOf(Props[Result], name = "Result")
  var sleep = false

  def receive = {
    case Init(filename) => run(filename)
    case Shutdown => shutdown()
  }

  def run(filename: String) = {
    println("SocialSim Initialized")

    val input = new Parser().readInput(filename)
    input foreach(_ match {
      case Models.CreateUser(user) => createUser(user)
      case Models.AddFollower(user, follower) => addFollower(user, follower)
      case Models.AddMessage(msg) => addMessage(msg)
    })

    while (Config.generateMessages) {
      val selectedUser = users.keySet.toList(Utils.random.nextInt(users.size))
      users(selectedUser) ! User.GenerateMessage
      Thread.sleep(Config.generatedMessagesSleepTime)
    }
  }

  def createUser(user: Models.User) = {
    users(user.id) = context.actorOf(Props(new User(user, result)), "user_" + user.id)
    Thread.sleep(5)
  }

  def addFollower(user: Models.User, follower: Models.User) = {
    users(follower.id) ! User.Follow(user, users(user.id))
    Thread.sleep(5)
  }

  def addMessage(msg: Models.Message) = {
    users(msg.owner.id) ! User.AddMessage(msg)
  }

  def shutdown() = {
    users.foreach {case (id, ref) => ref ! User.Ping }
    Thread.sleep(2000)
    context.system.shutdown()
  }
}
