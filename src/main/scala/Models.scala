object Models {
  trait IModel
  case class Tag(name: String, probability: Float = 0) extends IModel
  case class User(id: Int, name: String = "", tags: Array[Tag] = null) extends IModel
  case class Message(id: Int, owner: User, tags: Array[Tag]) extends IModel

  trait IAction
  case class CreateUser(user: Models.User) extends IAction
  case class AddFollower(user: Models.User, follower: Models.User) extends IAction
  case class AddMessage(msg: Message) extends IAction
}
