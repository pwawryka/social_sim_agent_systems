import java.io.File
import scala.io.Source

class Parser {

  def readInput(filename: String): List[Models.IAction] = {
    var input = List[Models.IAction]()
    val lines = Source.fromFile(new File(filename)).getLines()
    for (line <- lines) {
      val args = line.split("#")
      val action = args(0) match {
        case "create_user" => createUser(args)
        case "add_follower" => addFollower(args)
        case "add_message" => addMessage(args)
      }
      if (action != null) { input :+= action }
    }

    input
  }

  def createUser(args: Array[String]): Models.CreateUser = {
    val tags = args(3).split(",").map(tagData => {
      Models.Tag(tagData.split(" ")(0), tagData.split(" ")(1).toFloat)
    })
    Models.CreateUser(Models.User(args(1).toInt, args(2), tags))
  }

  def addFollower(args: Array[String]): Models.AddFollower = {
    Models.AddFollower(Models.User(args(1).toInt), Models.User(args(2).toInt))
  }

  def addMessage(args: Array[String]): Models.AddMessage = {
    val tags = args(2).split(",").map(Models.Tag(_))
    val owner = Models.User(args(1).toInt)
    val msg = Models.Message(Utils.nextMessageId, owner, tags)
    Models.AddMessage(msg)
  }
}
