import akka.actor._

object Result {
  case class Init(filename: String)
  case class UserRegister(user: Models.User)
  case class UserFollow(followed: Models.User, follower: Models.User)
  case class MessageShow(msg: Models.Message, user: Models.User)
  case class MessageAdd(msg: Models.Message)
}

class Result extends Actor {
  import Result._

  var filename = "output/example.txt"
  val writer = new FileResultWriter

  def receive = {
    case Init(fname) => filename = fname
    case UserRegister(user) => writer.write("user " + user.id + " " + user.name)
    case UserFollow(followed, follower) => writer.write("follow " + followed.id + " " + follower.id)
    case MessageAdd(msg) => writer.write("message_add " + msg.id + " " + msg.owner.id)
    case MessageShow(msg, user) => writer.write("message_show " + msg.id + " " + user.id)
  }


}
