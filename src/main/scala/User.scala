import scala.collection.mutable.MutableList
import akka.actor._

case class UserData(user: Models.User, userRef: ActorRef)

object User {
  case class Follow(user: Models.User, userRef: ActorRef)
  case class Followed(user: Models.User, userRef: ActorRef)
  case class AddMessage(msg: Models.Message)
  case class ShowMessage(msg: Models.Message)
  case object GenerateMessage

  case object Ping
}

class User(val user: Models.User, val result: ActorRef) extends Actor {
  import User._

  println(">>> initialized user " + user.id + " " + user.name)
  result ! Result.UserRegister(user)

  val followedBy = MutableList[UserData]()
  val following = MutableList[UserData]()
  val seenMessages = MutableList[Int]()

  def receive = {
    case Follow(user, userRef) => followUser(user, userRef)
    case Followed(user, userRef) => followedByUser(user, userRef)
    case AddMessage(msg) => addMessage(msg)
    case ShowMessage(msg) => showMessage(msg)
    case GenerateMessage => generateMessage

    case Ping => ping()
  }

  def ping() = {
    Thread.sleep(user.id * 100)
    println("User " + user.id)
    println("followed by:")
    for (u <- followedBy) { println(u.user.id) }
    println("following:")
    for (u <- following) { println(u.user.id) }
  }

  def followUser(user: Models.User, userRef: ActorRef) = {
    following += UserData(user, userRef)
    userRef ! Followed(this.user, self)
    result ! Result.UserFollow(user, this.user)
  }

  def followedByUser(user: Models.User, userRef: ActorRef) = {
    followedBy += UserData(user, userRef)
  }

  def addMessage(msg: Models.Message) = {
    result ! Result.MessageAdd(msg)
    seenMessages += msg.id
    for (userData <- followedBy) {
      userData.userRef ! ShowMessage(msg)
    }
  }

  def showMessage(msg: Models.Message) = {
    var found = false

    if (!seenMessages.contains(msg.id)) {
      for (userTag <- user.tags) {
        for (msgTag <- msg.tags) {
          if ((userTag.name == msgTag.name) && (userTag.probability < Utils.random.nextFloat())) found = true
        }
      }

      println(user.id + ": message received: " + msg.id + " " + msg.tags.map(_.name).mkString(" "))
      seenMessages += msg.id
      result ! Result.MessageShow(msg, user)
    }

    if (found) {
      println(user.id + ": i will send msg " + msg.id + " to others")
      for (userData <- followedBy) {
        userData.userRef ! ShowMessage(msg)
      }

//      if (Config.generateRelations && Config.relationProbability < Utils.random.nextFloat()) {
//        println(user.id + ": i am going to follow user " + msg.owner.id)
//        val ownerRef = context.actorSelection("").resolveOne().onComplete(
//          ownerRef => followUser(msg.owner, ownerRef.get)
//        )
//      }
    }
  }

  def generateMessage = {
    val tags = Array[Models.Tag] { user.tags(Utils.random.nextInt(user.tags.length)) }
    val msg = Models.Message(Utils.nextMessageId, user, tags)
    println(user.id + ": I am going to generate message " + msg)
    addMessage(msg)
  }
}
