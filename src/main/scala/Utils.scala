object Utils {
  val random = new java.util.Random

  private var messageCounter = -1
  def nextMessageId: Int = {
    messageCounter += 1
    messageCounter
  }

}
